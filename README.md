# Algebraic Tag Length Value encoding for Rust

[`atlv`](https://gitlab.com/adapt/atlv) is a generic binary data encoding
that preserves the data's leaf-spine structure
while providing customizable tags and dictating minimal semantics.

```
value:	binary | array | union
binary:	len=vlq(00) byte[len]
array:	len=vlq(01) value[len]
union:	tag=vlq(10) value
vlq(YY):	YYxxxxxx | 11xxxxxx vlq(YY)
byte:	xxxxxxxx
```

This repository contains a [Rust crate](https://crates.io/crates/atlv) for encoding, decoding, and generally manipulating `atlv` data.
