{ pkgs ? import <nixpkgs> {}
}:
{
  converge-shell = pkgs.stdenv.mkDerivation {
    name = "rust-shell";
    buildInputs = (with pkgs; [ rustc cargo rustfmt rust-analyzer ]);
  };
}
