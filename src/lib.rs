//! Encode and decode atlv values.
//!
//! Algebraic Tag Length Value (atlv) is a binary data encoding
//! designed for extensibility, generality, and compactness.
//! Each value has a flexible-length tag, and may be one of three [Kind]s,
//! one simple and two recursive:
//!
//! - Binaries for a sequence of bytes.
//! - Arrays for a sequence of values.
//! - Unions for a tagged single value.
//!
//! This crate provides
//! a [trait](Codec) to encode and decode rust values directly into atlv streams,
//! and a [type](Value) to model and genericly encode and decode arbitrary atlv values.
//! Use the trait unless you need to manipulate or examine unstructured atlv values.

mod generic;

mod codec;
mod error;
mod kind;

mod decode;
mod encode;
pub mod vlq;

#[cfg(feature = "quickcheck")]
mod quickcheck;

#[cfg(feature = "quickcheck")]
pub use crate::quickcheck::prop_enc_dec;

pub use crate::codec::Codec;
pub use crate::error::Error;
pub use crate::generic::Value;
pub use crate::kind::Kind;

pub use crate::decode::{decode, DecArray, DecBinary, DecHead, DecKind, Decoded, Decoder};
pub use crate::encode::{encode, EncArray, EncBinary, EncHead, Encoded, Encoder};

/// This is the maximum number of bytes of *binary* that the builtin `Codec` implementations will accept.
///
/// If you want something larger (or smaller) than this, use `dec_binary` or `dec_binary_tagged`.
pub const MAX_BINARY_LEN: usize = 16 * 1024 * 1024;

/// This is the maximum length of an array that the builtin `Codec` implementations will accept.
pub const MAX_ARRAY_LEN: usize = 1024 * 1024;
