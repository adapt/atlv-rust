extern crate alloc;

use crate::{DecHead, DecKind, Decoded, EncHead, Encoded, Error, MAX_ARRAY_LEN, MAX_BINARY_LEN};
use alloc::borrow::Cow;
use alloc::collections::{BTreeMap, BTreeSet, BinaryHeap, LinkedList, VecDeque};
use alloc::ffi::CString;
use alloc::rc::Rc;
use alloc::sync::Arc;
use core::cell::{Cell, OnceCell, RefCell};
use core::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::io::{self, Read, Write};

/// Types that are encodable and decodable with `atlv`.
///
/// Generally speaking implementations should target…
///
/// - `Binaries` for simple values: numbers, strings, cryptographic keys, bignums, or enums with a single such parametr.
/// - `Unions` for enum entries with a single complex (or parametric) parameter.
/// - `Arrays` for most structs, lists, vectors, arrays of complex types, maps, sets, and enum entries with multiple parameters.
pub trait Codec: Sized {
    /// Encode a value to an atlv stream.
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error>;

    /// Decode a value from an atlv stream.
    ///
    /// This is the primary method that callers of the trait will use for decoding.
    /// It automatically decodes the tag and includes the type name on errors.
    fn dec<R: io::Read>(stream: DecHead<R>) -> Result<Decoded<R, Self>, Error> {
        <Self>::decode(stream.dec_kind()?)
            .map_err(|e| Error::At(std::any::type_name::<Self>(), Box::new(e)))
    }

    /// Decode a value fom an atlv stream given the tag.
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error>;
}

impl<T: Codec> Codec for Box<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        T::enc(&self, stream)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        T::decode(stream).map(|x| x.map(Box::new))
    }
}

impl<'a, T: Codec + Clone> Codec for Cow<'a, T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        T::enc(&self, stream)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        T::decode(stream).map(|x| x.map(Cow::Owned))
    }
}

impl<T: Codec + Copy> Codec for Cell<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        T::enc(&self.get(), stream)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        T::decode(stream).map(|x| x.map(Cell::new))
    }
}

impl<T: Codec> Codec for RefCell<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        T::enc(&&self.try_borrow()?, stream)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        T::decode(stream).map(|x| x.map(RefCell::new))
    }
}

impl<T: Codec> Codec for OnceCell<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        match self.get() {
            None => stream.union(0, <()>::enc, &()),
            Some(x) => stream.union(1, Codec::enc, x),
        }
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let v = OnceCell::new();
        match stream.qty() {
            0 => Ok(stream.union(<()>::dec)?.map(|_| v)),
            1 => stream.union(Codec::dec)?.then(|a| {
                v.set(a).unwrap_or(());
                Ok(v)
            }),
            _ => Err(Error::WrongTag),
        }
    }
}

impl<T: Codec> Codec for Arc<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        T::enc(&self, stream)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        T::decode(stream).map(|x| x.map(Arc::new))
    }
}

impl<T: Codec> Codec for Rc<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        T::enc(&self, stream)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        T::decode(stream).map(|x| x.map(Rc::new))
    }
}

impl Codec for u8 {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(*self)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()
    }
}

impl Codec for u16 {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(*self)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()
    }
}

impl Codec for u32 {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(*self)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()
    }
}

impl Codec for u64 {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(*self)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()
    }
}

impl Codec for u128 {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(*self)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()
    }
}

impl Codec for usize {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(*self)
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()
    }
}

impl Codec for bool {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(if *self { 1u8 } else { 0 })
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()?.then(|x: u8| match x {
            0 => Ok(false),
            1 => Ok(true),
            _ => Err(Error::QuantityInvalid),
        })
    }
}

impl Codec for Ordering {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(match *self {
            Ordering::Less => 0,
            Ordering::Equal => 1,
            Ordering::Greater => 2u8,
        })
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.quantity()?.then(|x: u8| match x {
            0 => Ok(Ordering::Less),
            1 => Ok(Ordering::Equal),
            2 => Ok(Ordering::Greater),
            _ => Err(Error::QuantityInvalid),
        })
    }
}

impl<A: Codec> Codec for Option<A> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        match self {
            Option::None => stream.union(0, <()>::enc, &()),
            Option::Some(x) => stream.union(1, Codec::enc, x),
        }
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        match stream.qty() {
            0 => Ok(stream.union(<()>::dec)?.map(|_| Option::None)),
            1 => Ok(stream.union(Codec::dec)?.map(Option::Some)),
            _ => Err(Error::WrongTag),
        }
    }
}

impl<A: Codec, B: Codec> Codec for Result<A, B> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        match self {
            Err(x) => stream.union(0, Codec::enc, x),
            Ok(x) => stream.union(1, Codec::enc, x),
        }
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        match stream.qty() {
            0 => Ok(stream.union(Codec::dec)?.map(Err)),
            1 => Ok(stream.union(Codec::dec)?.map(Ok)),
            _ => Err(Error::WrongTag),
        }
    }
}

impl Codec for () {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.array(0)?.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream.array(0)?.done(())
    }
}

impl<A: Codec, B: Codec> Codec for (A, B) {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(2)?;
        arr = arr.item(Codec::enc, &self.0)?;
        arr = arr.item(Codec::enc, &self.1)?;
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let arr = stream.array(2)?;
        let (arr, a) = arr.item(Codec::dec)?;
        let (arr, b) = arr.item(Codec::dec)?;
        arr.done((a, b))
    }
}

impl<A: Codec, B: Codec, C: Codec> Codec for (A, B, C) {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(3)?;
        arr = arr.item(Codec::enc, &self.0)?;
        arr = arr.item(Codec::enc, &self.1)?;
        arr = arr.item(Codec::enc, &self.2)?;
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let arr = stream.array(3)?;
        let (arr, a) = arr.item(Codec::dec)?;
        let (arr, b) = arr.item(Codec::dec)?;
        let (arr, c) = arr.item(Codec::dec)?;
        arr.done((a, b, c))
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
impl<T: Codec> Codec for LinkedList<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for x in self {
            arr = arr.item(Codec::enc, x)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        let mut xs = Self::new();
        while arr.remaining() > 0 {
            let (arr1, x) = arr.item(Codec::dec)?;
            arr = arr1;
            xs.push_back(x);
        }
        arr.done(xs)
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
impl<T: Codec> Codec for VecDeque<T> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for x in self {
            arr = arr.item(Codec::enc, x)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        let mut xs = Self::with_capacity(arr.remaining());
        while arr.remaining() > 0 {
            let (arr1, x) = arr.item(Codec::dec)?;
            arr = arr1;
            xs.push_back(x);
        }
        arr.done(xs)
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
/// Warning! This does not check the order of decoded items, which may result in accepting non-canonical encodings.
impl<K: Codec + Ord> Codec for BinaryHeap<K> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for x in self {
            arr = arr.item(Codec::enc, x)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        let mut xs = Self::with_capacity(arr.remaining());
        while arr.remaining() > 0 {
            let (arr1, x) = arr.item(Codec::dec)?;
            arr = arr1;
            xs.push(x);
        }
        arr.done(xs)
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
/// Warning! This does not check the order of decoded items, which may result in accepting non-canonical encodings.
impl<K: Codec + Ord> Codec for BTreeSet<K> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for x in self {
            arr = arr.item(Codec::enc, x)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        let mut xs = Self::new();
        while arr.remaining() > 0 {
            let (arr1, x) = arr.item(Codec::dec)?;
            arr = arr1;
            xs.insert(x);
        }
        arr.done(xs)
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
/// Warning! This exposes the order of the entries in the hash table, if you're using an insecure hash this may expose you to hash based denial of service.
impl<K: Codec + Eq + Hash> Codec for HashSet<K> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for x in self {
            arr = arr.item(Codec::enc, x)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        let mut xs = Self::new();
        while arr.remaining() > 0 {
            let (arr1, x) = arr.item(Codec::dec)?;
            arr = arr1;
            xs.insert(x);
        }
        arr.done(xs)
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
/// Warning! This does not check the order of decoded items, which may result in accepting non-canonical encodings.
impl<K: Codec + Ord, V: Codec> Codec for BTreeMap<K, V> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for (k, v) in self {
            arr = arr.item(Codec::enc, k)?;
            arr = arr.item(Codec::enc, v)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        if arr.remaining() % 2 != 0 {
            return Err(Error::ArrayInvalidLength);
        }
        let mut xs = Self::new();
        while arr.remaining() > 0 {
            let (arr1, k) = arr.item(Codec::dec)?;
            let (arr2, v) = arr1.item(Codec::dec)?;
            arr = arr2;
            xs.insert(k, v);
        }
        arr.done(xs)
    }
}

/// Warning! This may read up to [MAX_ARRAY_LEN] items, which may introduce potential denial of service attacks.
/// Warning! This exposes the order of the entries in the hash table, if you're using an insecure hash this may expose you to hash based denial of service.
impl<K: Codec + Eq + Hash, V: Codec> Codec for HashMap<K, V> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut arr = stream.array(self.len())?;
        for (k, v) in self {
            arr = arr.item(Codec::enc, k)?;
            arr = arr.item(Codec::enc, v)?;
        }
        arr.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut arr = stream.array(MAX_ARRAY_LEN)?;
        if arr.remaining() % 2 != 0 {
            return Err(Error::ArrayInvalidLength);
        }
        let mut xs = Self::new();
        while arr.remaining() > 0 {
            let (arr1, k) = arr.item(Codec::dec)?;
            let (arr2, v) = arr1.item(Codec::dec)?;
            arr = arr2;
            xs.insert(k, v);
        }
        arr.done(xs)
    }
}

impl Codec for Vec<u8> {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut bin = stream.binary(self.len())?;
        bin.write_all(&self)?;
        bin.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut bin = stream.binary(MAX_BINARY_LEN)?;
        let buf = bin.read_vec(bin.remaining())?;
        bin.done(buf)
    }
}

impl<const N: usize> Codec for [u8; N] {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let mut bin = stream.binary(self.len())?;
        bin.write_all(self)?;
        bin.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut bin = stream.binary(N)?;
        let mut buf = [0; N];
        bin.read_exact(&mut buf)?;
        bin.done(buf)
    }
}

impl Codec for char {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        stream.quantity(u32::from(*self))
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        stream
            .quantity()?
            .then(|v: u32| Self::try_from(v).map_err(Error::from))
    }
}

impl Codec for String {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let bytes = self.as_bytes();
        let mut bin = stream.binary(self.len())?;
        bin.write_all(bytes)?;
        bin.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut bin = stream.binary(MAX_BINARY_LEN)?;
        let buf = bin.read_vec(bin.remaining())?;
        let str = Self::from_utf8(buf)?;
        bin.done(str)
    }
}

impl Codec for CString {
    fn enc<W: io::Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        let bytes = self.as_bytes();
        let mut bin = stream.binary(self.as_bytes().len())?;
        bin.write_all(bytes)?;
        bin.done()
    }
    fn decode<R: io::Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        let mut bin = stream.binary(MAX_BINARY_LEN)?;
        let buf = bin.read_vec(bin.remaining())?;
        let str = Self::new(buf)?;
        bin.done(str)
    }
}
