use crate::{decode::decode, encode::encode, Codec};
use quickcheck::{Arbitrary, QuickCheck};

/// Generate arbitrary values of a type and test that encoding then decoding results in the original value.
pub fn prop_enc_dec<T: Arbitrary + Codec + Clone + Eq + std::fmt::Debug>() {
    fn encdec<T: Arbitrary + Codec + Clone + Eq + std::fmt::Debug>(x: T) -> bool {
        let mut v = <Vec<u8>>::new();
        v = encode(Codec::enc, &x, v).unwrap();
        print!("raw: {:?}\n", v);
        let (_, y) = decode(v.as_slice(), Codec::dec).unwrap();
        print!("recooked: {:?}\n", y);
        x == y
    }
    QuickCheck::new().quickcheck(encdec as fn(T) -> bool);
}
