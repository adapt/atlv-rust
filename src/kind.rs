/// The different kinds of `atlv` values.
#[derive(Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Debug, Hash)]
pub enum Kind {
    /// Binaries are designed for bulk binary data.
    ///
    /// This is a good choice for strings, cryptographic keys, and all other sorts of raw data.
    Binary,
    /// Unions wrap a single other value.
    ///
    /// The prototypical use case here is are the `Option` and `Err` types,
    /// but any `enum` type where the subtypes are not coordinated will benefit.
    Union,
    /// Arrays encode lists, vectors, arrays of complex objects, maps, sets, etc.
    ///
    /// Most non-trivial 'struct' types should be encoded as arrays.
    Array,
}

impl From<u8> for Kind {
    fn from(x: u8) -> Kind {
        match x & 3 {
            0 => Kind::Binary,
            1 => Kind::Array,
            _ => Kind::Union,
        }
    }
}

impl From<Kind> for u8 {
    fn from(x: Kind) -> u8 {
        match x {
            Kind::Binary => 0,
            Kind::Array => 1,
            Kind::Union => 2,
        }
    }
}

pub struct KindTag<T>(pub T, pub Kind);
