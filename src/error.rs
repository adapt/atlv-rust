extern crate alloc;

use std::io;
use std::str;
use std::string;

/// Errors that can come up while parsing.
#[derive(Debug, Eq, PartialEq)]
pub enum Error {
    /// The wrong tag was provided.
    WrongTag,
    /// The wrong type was provided.
    WrongKind,
    /// The encoded quantity doesn't fit in the target type.
    QuantityTooLarge,
    /// The encoded quantity isn't a known value.
    QuantityInvalid,
    /// Attempted to parse a binary that exceeds the imposed limit.
    BinaryTooLong,
    /// Attepected to parse a binary that is otherwise the wrong length.
    BinaryInvalidLength,
    /// Attempted to parse an array that exceeds the imposed limit.
    ArrayTooLong,
    /// Attempted to parse an array that is otherwise the wrong length.
    ArrayInvalidLength,
    /// An IO error occurred during reading.
    IO(io::ErrorKind),
    /// Tried to create an invalid Unicode character (that is `0xd800..=0xdfff` or `0x110000..`).
    InvalidUnicodeCharacter,
    /// An error during parsing UTF-8.
    InvalidUtf8,
    /// The value provided was invalid for the target type.
    InvalidValue,
    /// Could not borrow the value to encode it.
    BorrowFailed,
    /// A path marker.
    At(&'static str, Box<Error>),
    /// An index marker.
    Offset(usize, Box<Error>),
}

impl From<core::cell::BorrowError> for Error {
    fn from(_: core::cell::BorrowError) -> Error {
        Error::BorrowFailed
    }
}

impl From<alloc::ffi::NulError> for Error {
    fn from(_: alloc::ffi::NulError) -> Error {
        Error::InvalidValue
    }
}

impl From<std::char::CharTryFromError> for Error {
    fn from(_: std::char::CharTryFromError) -> Error {
        Error::InvalidUnicodeCharacter
    }
}

impl From<str::Utf8Error> for Error {
    fn from(_: str::Utf8Error) -> Error {
        Error::InvalidUtf8
    }
}

impl From<string::FromUtf8Error> for Error {
    fn from(_: string::FromUtf8Error) -> Error {
        Error::InvalidUtf8
    }
}

impl From<io::Error> for Error {
    fn from(x: io::Error) -> Error {
        Error::IO(x.kind())
    }
}

impl From<io::ErrorKind> for Error {
    fn from(x: io::ErrorKind) -> Error {
        Error::IO(x)
    }
}
