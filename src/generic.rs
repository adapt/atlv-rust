//! Generic Algebraic Type Length Values

use crate::{
    Codec, DecKind, Decoded, EncHead, Encoded, Error, Kind, MAX_ARRAY_LEN, MAX_BINARY_LEN,
};
use std::io::{Read, Write};
use std::vec::Vec;

#[cfg(feature = "quickcheck")]
use quickcheck::Arbitrary;

#[cfg(all(feature = "quickcheck", test))]
use crate::prop_enc_dec;

/// A generic representation of an `atlv` value.
///
/// While the encoding properly doesn't have any hard limits,
/// this generic implementation does -
/// the tags and length of binaries and arrays must fit into usize.
#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Clone, Hash)]
pub enum Value {
    /// Binary data, byte strings, text…
    Binary(Vec<u8>),
    /// Tagged Unions, Sum Types, rust enums…
    Union(usize, Box<Value>),
    /// Arrays, Product Types…
    Array(Vec<Value>),
}

impl Codec for Value {
    fn enc<W: Write>(&self, stream: EncHead<W>) -> Result<Encoded<W>, Error> {
        match self {
            Value::Binary(v) => {
                let mut bin = stream.binary(v.len())?;
                bin.write_all(&v)?;
                bin.done()
            }
            Value::Union(t, v) => stream.union(*t, Codec::enc, v),
            Value::Array(v) => {
                let mut arr = stream.array(v.len())?;
                for x in v {
                    arr = arr.item(Codec::enc, x)?;
                }
                arr.done()
            }
        }
    }

    fn decode<R: Read>(stream: DecKind<R>) -> Result<Decoded<R, Self>, Error> {
        match stream.kind() {
            Kind::Binary => {
                let mut bin = stream.binary(MAX_BINARY_LEN)?;
                let buf = bin.read_vec(bin.remaining())?;
                bin.done(Value::Binary(buf))
            }
            Kind::Union => {
                let tag = stream.qty();
                Ok(stream
                    .union(Codec::dec)?
                    .map(|x| Value::Union(tag, Box::new(x))))
            }
            Kind::Array => {
                let mut arr = stream.array(MAX_ARRAY_LEN)?;
                let mut vec = Vec::with_capacity(arr.remaining());
                while arr.remaining() > 0 {
                    let (arr1, x) = arr.item(Codec::dec)?;
                    arr = arr1;
                    vec.push(x);
                }
                arr.done(Value::Array(vec))
            }
        }
    }
}

impl Value {
    pub fn kind(self) -> Kind {
        match self {
            Value::Binary(_) => Kind::Binary,
            Value::Union(_, _) => Kind::Union,
            Value::Array(_) => Kind::Array,
        }
    }
}

#[cfg(feature = "quickcheck")]
impl Arbitrary for Value {
    fn arbitrary(g: &mut quickcheck::Gen) -> Self {
        if g.size() == 1 {
            return Value::Binary(vec![]);
        }
        let mut gen = quickcheck::Gen::new(g.size() / 2);
        match u8::arbitrary(g) % 4 {
            0 => Value::Binary(Arbitrary::arbitrary(g)),
            1 => Value::Union(Arbitrary::arbitrary(g), Arbitrary::arbitrary(&mut gen)),
            _ => Value::Array(Arbitrary::arbitrary(&mut gen)),
        }
    }
}

#[cfg(feature = "quickcheck")]
#[test]
fn prop_value_enc_dec() {
    prop_enc_dec::<Value>()
}
