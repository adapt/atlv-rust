//! This module rexports the relevant pieces [vlq_bij] relevant to atlv.
//!
//! Callers of atlv may implement [Vlq128] to implement custom tag type.
use crate::kind::Kind;
use crate::kind::KindTag;
use std::io::{Read, Write};
pub use vlq_bij::be128::{decode, encode, Error, Vlq128};

impl From<Error> for crate::Error {
    fn from(value: Error) -> crate::Error {
        match value {
            Error::Overflow => crate::Error::QuantityTooLarge,
            Error::IO(err) => crate::Error::IO(err.kind()),
        }
    }
}

/// Write a base-256 big-endian bijective variable length quantity to `writer`.
///
/// The length must be encoded seperately.
/// This makes it suitable for encoding quantities in [Kind::Binary] values.
///
/// - Write errors are returned.
pub fn enc8<A: Vlq128>(mut writer: impl Write, value: &A) -> Result<usize, Error> {
    let mut buf = Vec::new();
    let (mut opt_state, mut val) = value.enc_vlq_final(8)?;
    buf.push(val);
    while let Some(state) = opt_state {
        (opt_state, val) = A::enc_vlq_step(state, 8)?;
        buf.push(val);
    }
    buf.reverse();
    writer.write_all(&buf)?;
    return Ok(buf.len());
}

/// Read a base-128 big-endian bijective variable length quantity from `reader`.
///
/// - Read errors are returned.
/// - Wraparound overflows are caught and return `Error::Overflow`.
pub fn dec8<A: Vlq128>(mut reader: impl Read, len: usize) -> Result<A, Error> {
    let mut buf = [0];
    let mut state = <A>::dec_vlq_init(buf[0], 0)?;
    while len > 0 {
        reader.read_exact(&mut buf)?;
        state = A::dec_vlq_step(state, buf[0], 8)?;
    }
    return A::dec_vlq_final(state);
}

/// Write a base-64 big-endian bijective variable length quantity to `writer`, along with a [Kind].
///
/// - Write errors are returned.
pub fn enc6kind<A: Vlq128>(mut writer: impl Write, value: &KindTag<A>) -> Result<usize, Error> {
    let mut buf = Vec::new();
    let (mut opt_state, mut val) = value.0.enc_vlq_final(6)?;
    buf.push((u8::from(value.1) << 6) | (val & 0x3fu8));
    while let Some(state) = opt_state {
        (opt_state, val) = A::enc_vlq_step(state, 6)?;
        buf.push(val | 0xc0);
    }
    buf.reverse();
    writer.write_all(&buf)?;
    return Ok(buf.len());
}

/// Read a base-128 big-endian bijective variable length quantity from `reader`.
///
/// - Read errors are returned.
/// - Wraparound overflows are caught and return `Error::Overflow`.
pub fn dec6kind<A: Vlq128>(mut reader: impl Read) -> Result<KindTag<A>, Error> {
    let mut buf = [0];
    reader.read_exact(&mut buf)?;
    let mut state = <A>::dec_vlq_init(buf[0] & 0x3f, 6)?;
    loop {
        if buf[0] & 0xc0 != 0xc0 {
            let kind = Kind::from(buf[0] >> 6);
            return A::dec_vlq_final(state).map(|x| KindTag(x, kind));
        }
        reader.read_exact(&mut buf)?;
        state = A::dec_vlq_step(state, buf[0] & 0x3f, 6)?;
    }
}

#[cfg(test)]
const TEST_TAGS: &[(u32, Kind, &[u8])] = &[
    (0, Kind::Binary, &[0x0]),
    (63, Kind::Binary, &[0x3f]),
    (63, Kind::Array, &[0x7f]),
    (63, Kind::Union, &[0xbf]),
    (64, Kind::Binary, &[0xc0, 0x00]),
    (4160, Kind::Binary, &[0xc0, 0xc0, 0x00]),
    (
        0xffffffff,
        Kind::Union,
        &[0xc2, 0xfe, 0xfe, 0xfe, 0xfe, 0xbf],
    ),
];

#[test]
fn read_sample_tags() {
    for t in TEST_TAGS {
        let KindTag::<u32>(tag, ty) = dec6kind(t.2).unwrap();
        assert_eq!(tag, t.0);
        assert_eq!(ty, t.1);
    }
}

#[test]
fn write_sample_tags() {
    for t in TEST_TAGS {
        let mut buf: Vec<u8> = Vec::new();
        enc6kind(&mut buf, &KindTag(t.0, t.1)).unwrap();
        assert_eq!(t.2, buf);
    }
}
