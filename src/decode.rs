use crate::{
    kind::KindTag,
    vlq::{self, Vlq128},
    Error, Kind,
};
use std::io::{self, Read};

/// A function that decodes a single value from an atlv stream.
///
/// The stream type is constrained to ensure a valid decoding.
/// Use [decode] to decode this from anything that implements [io::Read].
pub type Decoder<R, A> = fn(stream: DecHead<R>) -> Result<Decoded<R, A>, Error>;

/// Decode a value from some reader.
pub fn decode<R: Read, A>(reader: R, dec: Decoder<R, A>) -> Result<(R, A), Error> {
    let decoded = dec(DecHead(reader))?;
    Ok((decoded.reader, decoded.value))
}

/// A decoding atlv stream at the end of a value, and the value that was decoded.
///
/// This should be returned to the parent decoder, but the embedded value can also be manpiulated first.
pub struct Decoded<R, A> {
    reader: R,
    value: A,
}

impl<R, A> Decoded<R, A> {
    /// Apply a function to the embedded value.
    ///
    /// This is especially useful for decoding enum entries with a single parameter.
    pub fn map<B>(self, f: impl FnOnce(A) -> B) -> Decoded<R, B> {
        Decoded {
            reader: self.reader,
            value: f(self.value),
        }
    }

    /// Apply a function that might result in failure to the embedded value.
    ///
    /// This is especially useful for further restricting the domain of parsed value.
    /// Like an enum without parameters that's encoded as a quantity.
    pub fn then<B>(self, f: impl FnOnce(A) -> Result<B, Error>) -> Result<Decoded<R, B>, Error> {
        Ok(Decoded {
            reader: self.reader,
            value: f(self.value)?,
        })
    }
}

/// A decoding stream at the beginning of an atlv value.
///
/// There's nothing much to do here but decode the kind.
pub struct DecHead<R>(R);

impl<R: Read> DecHead<R> {
    /// Decode the kind (and length or tag), returning an [DecKind].
    pub fn dec_kind(self) -> Result<DecKind<R>, Error> {
        let mut reader = self.0;
        let KindTag(qty, kind) = vlq::dec6kind(&mut reader)?;
        Ok(DecKind { qty, kind, reader })
    }
}

/// A stream just after the kind (and quantity) have been decoded.
///
/// This is the primary decision-making point during decoding.
/// The kind (and tag if a union) allow the decoder to branch,
/// determining what decoder to invoke inside.
/// That is particurarly relevant to enum types,
/// but is also important for validating the data.
pub struct DecKind<R> {
    qty: usize,
    kind: Kind,
    reader: R,
}

impl<R> DecKind<R> {
    /// Get the decoded kind.
    pub fn kind(&self) -> Kind {
        self.kind
    }

    /// Test for a specific kind.
    pub fn check_kind(&self, kind: Kind) -> Result<(), Error> {
        if self.kind != kind {
            Err(Error::WrongKind)
        } else {
            Ok(())
        }
    }

    /// Get the tag (if it's a tagged union) or length (for binary/array).
    pub fn qty(&self) -> usize {
        self.qty
    }

    /// Test for a specific tag (or length).
    pub fn check_qty(&self, qty: usize) -> Result<(), Error> {
        if qty != self.qty {
            Err(Error::WrongTag)
        } else {
            Ok(())
        }
    }
}

impl<R: Read> DecKind<R> {
    /// Decode a base-256 variable length quantity stored inside a binary value.
    pub fn quantity<T: Vlq128>(self) -> Result<Decoded<R, T>, Error> {
        let mut bin = self.binary(64)?;
        let len = bin.remaining();
        let val = vlq::dec8(&mut bin, len)?;
        bin.done(val)
    }

    /// Start decoding a binary value.
    ///
    /// This yields a [DecBinary], which provides for reading the content.
    pub fn binary(self, max_len: usize) -> Result<DecBinary<R>, Error> {
        self.check_kind(Kind::Binary)?;
        let reader = self.reader;
        let len = self.qty;
        let count = len;

        if len > max_len {
            Err(Error::BinaryTooLong)
        } else {
            Ok(DecBinary { count, len, reader })
        }
    }

    /// Decode a union with a particular decoder.
    pub fn union<A>(self, dec: Decoder<R, A>) -> Result<Decoded<R, A>, Error> {
        self.check_kind(Kind::Union)?;
        dec(DecHead(self.reader))
    }

    /// Start decoding an array value.
    ///
    /// This yields a [DecArray], which provides for reading the content.
    pub fn array(self, max_count: usize) -> Result<DecArray<R>, Error> {
        self.check_kind(Kind::Array)?;
        let reader = self.reader;
        let len = self.qty;
        let count = len;

        if len > max_count {
            Err(Error::ArrayTooLong)
        } else {
            Ok(DecArray { count, len, reader })
        }
    }
}

/// A stream in the process of decoding a binary value.
///
/// This implements [Read], and additionally supplies [DecBinary::read_vec],
/// to quickly pull out a new byte vector of a particular size.
///
/// When all the bytes have been consumed, use [DecBinary::done] to return.
///
/// This will not allow you to read beyond the end of the buffer,
/// and will error if you attempt to do so.
/// Attempting to end early will likewise error.
pub struct DecBinary<R> {
    count: usize,
    len: usize,
    reader: R,
}

impl<R: Read> Read for DecBinary<R> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        if buf.len() > self.len {
            Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                "Read beyond specified length.",
            ))
        } else {
            let got = self.reader.read(buf)?;
            self.len -= got;
            Ok(got)
        }
    }
}

impl<R: Read> DecBinary<R> {
    /// Read some number of bytes into a new byte vector.
    pub fn read_vec(&mut self, n: usize) -> Result<Vec<u8>, Error> {
        let mut buf = Vec::with_capacity(n);
        // Set the length so that the reader reads the whole thing.
        unsafe { buf.set_len(n) };
        self.read_exact(&mut buf)?;
        Ok(buf)
    }

    /// Read the remaining bytes into a new byte vector.
    pub fn read_all(&mut self) -> Result<Vec<u8>, Error> {
        self.read_vec(self.len)
    }
}

impl<R> DecBinary<R> {
    /// Get the number of bytes remaining in the binary.
    ///
    /// This is mostly useful at the beginning,
    /// to read the full binary at once.
    pub fn remaining(&self) -> usize {
        self.len
    }

    /// Finish up working with the binary.
    ///
    /// This is usually the return value of the decoding function.
    ///
    /// Note that this will fail unless all the data has been read.
    pub fn done<A>(self, res: A) -> Result<Decoded<R, A>, Error> {
        if self.len == 0 {
            Ok(Decoded {
                reader: self.reader,
                value: res,
            })
        } else {
            Err(Error::Offset(
                self.count - self.len,
                Box::new(Error::BinaryInvalidLength),
            ))
        }
    }
}

/// A stream in the process of decoding an array of values.
///
/// Use [DecArray::item] to parse each item in turn.
///
/// When all the contained values have been consumed, use [DecArray::done] to return.
/// You must consume the precise number of values.
/// If you have a variable number of values, use [DecArray::remaining] to count them.
pub struct DecArray<R> {
    count: usize,
    len: usize,
    reader: R,
}

impl<R> DecArray<R> {
    /// Get the number of items remaining in the array.
    pub fn remaining(&self) -> usize {
        self.len
    }

    /// Decode the next item of the array with a particular decoder.
    pub fn item<A>(self, dec: Decoder<R, A>) -> Result<(DecArray<R>, A), Error> {
        if self.len == 0 {
            return Err(Error::ArrayInvalidLength);
        }
        let decoded = dec(DecHead(self.reader))?;
        Ok((
            DecArray {
                count: self.count,
                len: self.len - 1,
                reader: decoded.reader,
            },
            decoded.value,
        ))
    }

    /// Finish decoding an array.
    ///
    /// Note that this will fail unless all the items in the array have been consumed.
    pub fn done<A>(self, res: A) -> Result<Decoded<R, A>, Error> {
        if self.len == 0 {
            Ok(Decoded {
                reader: self.reader,
                value: res,
            })
        } else {
            Err(Error::Offset(
                self.count - self.len,
                Box::new(Error::ArrayInvalidLength),
            ))
        }
    }
}
