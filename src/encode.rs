use crate::{
    kind::KindTag,
    vlq::{self, Vlq128},
    Error, Kind,
};
use std::io::{self, Write};

/// A stream after encoding a value.
pub struct Encoded<W>(W);

/// A function that encodes a single value to an atlv stream.
///
/// The stream type is constrained to ensure a valid encoding.
/// Use [encode] to encode this to anything that implements [Write].
pub type Encoder<W, A> = fn(value: &A, stream: EncHead<W>) -> Result<Encoded<W>, Error>;

/// Encode a value to some writer.
pub fn encode<W, A>(enc: Encoder<W, A>, value: &A, writer: W) -> Result<W, Error> {
    let encoded = enc(value, EncHead(writer))?;
    Ok(encoded.0)
}

/// An encoding atlv stream at the beginning of a value.
///
/// This is the main point where the encoder decides what kind of value to write.
pub struct EncHead<W>(W);

impl<W: Write> EncHead<W> {
    pub fn quantity<T: Vlq128>(self, qty: T) -> Result<Encoded<W>, Error> {
        let mut buf = Vec::new();
        vlq::enc8(&mut buf, &qty)?;
        let mut bin = self.binary(buf.len())?;
        bin.write_all(&buf)?;
        bin.done()
    }

    /// Encode the length of a binary value.
    ///
    /// Returns a binary encoder to encode the content.
    pub fn binary(self, len: usize) -> Result<EncBinary<W>, Error> {
        let mut writer = self.0;
        vlq::enc6kind(&mut writer, &KindTag(len, Kind::Binary))?;
        Ok(EncBinary {
            writer,
            count: len,
            len,
        })
    }

    /// Encode the tag and a value of a union.
    pub fn union<A>(self, tag: usize, enc: Encoder<W, A>, value: &A) -> Result<Encoded<W>, Error> {
        let mut writer = self.0;
        vlq::enc6kind(&mut writer, &KindTag(tag, Kind::Union))?;
        enc(value, EncHead(writer))
    }

    /// Encode the length of an array value.
    ///
    /// Returns an array encoder to encode the items.
    pub fn array(self, len: usize) -> Result<EncArray<W>, Error> {
        let mut writer = self.0;
        vlq::enc6kind(&mut writer, &KindTag(len, Kind::Array))?;
        Ok(EncArray {
            count: len,
            len,
            writer,
        })
    }
}

/// A stream in the midst of encoding a binary value.
///
/// This implements [Write], to encode the content.
///
/// When all the bytes have been written, use [EncBinary::done] to return.
///
/// This will not allow you to write beyond the stated length,
/// and will error if you attempt to do so.
/// Attempting to end early will likewise error.
pub struct EncBinary<W> {
    writer: W,
    count: usize,
    len: usize,
}

impl<W: Write> Write for EncBinary<W> {
    fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
        if self.len < buf.len() {
            Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                "Attempted to write beyond stated length.",
            ))
        } else {
            let written = self.writer.write(buf)?;
            self.len -= written;
            Ok(written)
        }
    }

    fn flush(&mut self) -> Result<(), io::Error> {
        self.writer.flush()
    }
}

impl<W> EncBinary<W> {
    /// Finish encoding a binary value.
    pub fn done(self) -> Result<Encoded<W>, Error> {
        if self.len == 0 {
            Ok(Encoded(self.writer))
        } else {
            Err(Error::Offset(
                self.count - self.len,
                Box::new(Error::BinaryInvalidLength),
            ))
        }
    }
}

/// A stream in midst of encoding an array of values.
///
/// Use [EncArray::item] to encode each item in turn.
///
/// When all the contained values have been encoded, use [EncArray::done] to return.
/// You must encode the precise number of values stated in the header.
pub struct EncArray<W> {
    writer: W,
    count: usize,
    len: usize,
}

impl<W> EncArray<W> {
    /// Get the number of items remaining to encode.
    pub fn remaining(&self) -> usize {
        self.len
    }

    /// Encode the next item of the array.
    pub fn item<A>(self, enc: Encoder<W, A>, value: &A) -> Result<EncArray<W>, Error> {
        if self.len == 0 {
            return Err(Error::ArrayInvalidLength);
        }
        let encoded = enc(value, EncHead(self.writer))?;
        Ok(EncArray {
            count: self.count,
            len: self.len - 1,
            writer: encoded.0,
        })
    }

    /// Finish encoding an array.
    ///
    /// Note that this will fail if the array is not the stated size.
    pub fn done(self) -> Result<Encoded<W>, Error> {
        if self.len == 0 {
            Ok(Encoded(self.writer))
        } else {
            Err(Error::Offset(
                self.count - self.len,
                Box::new(Error::ArrayInvalidLength),
            ))
        }
    }
}
